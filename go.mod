module gitlab.com/gitlab-org/gitlab-pages

go 1.12

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fzipp/gocyclo v0.0.0-20150627053110-6acd4345c835
	github.com/golang/mock v1.3.1
	github.com/gorilla/context v1.1.1
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/securecookie v1.1.1
	github.com/gorilla/sessions v1.2.0
	github.com/kardianos/osext v0.0.0-20190222173326-2bc1f35cddc0
	github.com/karrick/godirwalk v1.10.12
	github.com/namsral/flag v1.7.4-pre
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/prometheus/client_golang v1.1.0
	github.com/rs/cors v1.7.0
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.4.0
	github.com/tomasen/realip v0.0.0-20180522021738-f0c99a92ddce
	github.com/wadey/gocovmerge v0.0.0-20160331181800-b5bfa59ec0ad
	gitlab.com/gitlab-org/labkit v0.0.0-20200414155917-f06e28fff6fa
	gitlab.com/lupine/go-mimedb v0.0.0-20180307000149-e8af1d659877
	golang.org/x/crypto v0.0.0-20191011191535-87dc89f01550
	golang.org/x/lint v0.0.0-20191125180803-fdd1cda4f05f
	golang.org/x/net v0.0.0-20200114155413-6afb5195e5aa
	golang.org/x/sys v0.0.0-20200113162924-86b910548bc1
	golang.org/x/tools v0.0.0-20200117161641-43d50277825c
	gopkg.in/yaml.v2 v2.2.4
)

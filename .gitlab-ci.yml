include:
  - template: Security/License-Scanning.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml

stages:
  - prepare
  - test

workflow:
  rules:
    # For merge requests, create a pipeline.
    - if: '$CI_MERGE_REQUEST_IID'
    # For `master` branch, create a pipeline (this includes on schedules, pushes, merges, etc.).
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    # For tags, create a pipeline.
    - if: '$CI_COMMIT_TAG'
    # For stable, and security branches, create a pipeline.
    - if: '$CI_COMMIT_BRANCH =~ /^[\d-]+-stable(-ee)?$/'
    - if: '$CI_COMMIT_BRANCH =~ /^security\//'

default:
  image: golang:1.12
  tags:
    - gitlab-org

.go-mod-cache:
  variables:
    GOPATH: $CI_PROJECT_DIR/.GOPATH
  before_script:
    - mkdir -p .GOPATH
  cache:
    paths:
      - .GOPATH/pkg/mod/

.tests:
  extends: .go-mod-cache
  stage: test
  tags:
    - gitlab-org-docker
  needs: ['download deps']
  script:
  - echo "Running all tests without daemonizing..."
  - make test
  - echo "Running just the acceptance tests daemonized (tmpdir)...."
  - TEST_DAEMONIZE=tmpdir make acceptance
  - echo "Running just the acceptance tests daemonized (inplace)...."
  - TEST_DAEMONIZE=inplace make acceptance
  artifacts:
    paths:
      - bin/gitlab-pages

license_scanning:
  stage: prepare
  variables:
    LICENSE_MANAGEMENT_SETUP_CMD: go mod vendor
  rules:
    - if: $CI_MERGE_REQUEST_ID
      when: on_success
    - if: $CI_COMMIT_BRANCH == 'master'
      when: on_success

secrets-sast:
  stage: prepare
  rules:
    - if: $CI_MERGE_REQUEST_ID
      when: on_success
    - if: $CI_COMMIT_BRANCH == 'master'
      when: on_success

gosec-sast:
  stage: prepare
  rules:
    - if: $CI_MERGE_REQUEST_ID
      when: on_success
    - if: $CI_COMMIT_BRANCH == 'master'
      when: on_success

download deps:
  extends: .go-mod-cache
  stage: prepare
  script:
    - make deps-download
  artifacts:
    paths:
      - go.mod
      - go.sum

verify:
  extends: .go-mod-cache
  stage: test
  needs: ['download deps']
  script:
    - make setup
    - make generate-mocks
    - make verify
    - make cover
  artifacts:
    paths:
      - coverage.html

test:1.12:
  extends: .tests
  image: golang:1.12

test:1.13:
  extends: .tests
  image: golang:1.13

test:1.14:
  extends: .tests
  image: golang:1.14

race:
  extends: .go-mod-cache
  stage: test
  tags:
    - gitlab-org-docker
  needs: ['download deps']
  script:
  - echo "Running race detector"
  - make race

check deps:
  extends: .go-mod-cache
  stage: test
  needs: ['download deps']
  script:
    - make deps-check
